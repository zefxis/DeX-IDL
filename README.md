To enable interconnection of heterogeneous Services/Things, additional softwares or artifacts are required. 
The  Data eXchange Interface Description Language that we call DeX-IDL Model enables to describe Service/Thing  with parameters such as : 

- operartions provided by specifing the intraction type(one way/two way) and the role (consummer/provider)
- names and data types of theses  operartions
- IP address and port number to reach this Service/Thing

Using the DeX-IDL model, developpers are able to automate synthesis artifacts to enable interconnection of heterogeneous Services/Things.


This project provides an eclipse plugin to enable developper to create DEXIDL model in an ease way. 
To install the DEXIDL plugin in Eclipse IDE,  developper can use the following site http://mimove-apps.paris.inria.fr:11080/content/sites/dexidl/

To install in Eclipse, go to Help > Install New Software. This displays the Available Software screen, press the Add button. This displays the Add Repository window. Specify the name and Location as depict bellow, press Ok button and select all available Software. Read the license agreements and then select I accept the terms of the license agreements. Click Finish.

![DeX-IDL Plug-in installation](https://gitlab.inria.fr/zefxis/IoT-web-console/raw/master/IoTwebconsole/WebContent/css/images/InstallGIDL.png)


To create DeX-IDL model in Eclipse, go to File > New > Other. This displays the selection wizards screen, select Example EMF Model creation wizards > GIDL Model, press next to fill the name of the GIDL Model and press next. This displays Gidl Model screen. Select "Gidl Model" as Model Object and "UTF-8" the XML encoding. Press next to complete the creation process.

![DeX-IDL Model creation](https://gitlab.inria.fr/zefxis/IoT-web-console/raw/master/IoTwebconsole/WebContent/css/images/CreateGIDL.png)

   









