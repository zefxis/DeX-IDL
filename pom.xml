<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright 2015 The CHOReVOLUTION project

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
   
   <modelVersion>4.0.0</modelVersion>
   <groupId>eu.chorevolution.modelingnotations</groupId>
   <artifactId>eu.chorevolution.modelingnotations.parent</artifactId>
   <version>2.1.1-SNAPSHOT</version>
   <name>GIDL Model</name>
   <description>General Interface Description Language Model</description>
   
   <packaging>pom</packaging>

   <prerequisites>
      <maven>3.0</maven>
   </prerequisites>
   
   <organization>
      <name>The CHOReVOLUTION project</name>
      <url>http://www.chorevolution.eu</url>
   </organization>

   <developers>
        <developer>
            <id>marco_autili</id>
            <name>Marco Autili</name>
            <email>marco.autili@univaq.it</email>
            <url>http://www.di.univaq.it/marco.autili/</url>
            <organization>University of LAquila</organization>
            <organizationUrl>http://www.univaq.it/en/</organizationUrl>
            <roles>
                <role>architect</role>
            </roles>
            <timezone>Italy/Rome</timezone>
        </developer>
        <developer>
            <id>massimo_tivoli</id>
            <name>Massimo Tivoli</name>
            <email>massimo.tivoli@di.univaq.it</email>
            <url>http://massimotivoli.wixsite.com/home/</url>
            <organization>University of LAquila</organization>
            <organizationUrl>http://www.univaq.it/en/</organizationUrl>
            <roles>
                <role>architect</role>
            </roles>
            <timezone>Italy/Rome</timezone>
        </developer>
        <developer>
            <id>amleto_disalle</id>
            <name>Amleto Di Salle</name>
            <email>amleto.disalle@univaq.it</email>
            <url>http://www.amletodisalle.it/</url>
            <organization>University of LAquila</organization>
            <organizationUrl>http://www.univaq.it/en/</organizationUrl>
            <roles>
                <role>architect</role>
            </roles>
            <timezone>Italy/Rome</timezone>
        </developer>
        <developer>
            <id>alexander_perucci</id>
            <name>Alexander Perucci</name>
            <email>alexander.perucci@gmail.com</email>
            <url>http://www.alexanderperucci.com/</url>
            <organization>University of LAquila</organization>
            <organizationUrl>http://www.univaq.it/en/</organizationUrl>
            <roles>
                <role>architect</role>
                <role>developer</role>
            </roles>
            <timezone>Italy/Rome</timezone>
        </developer>
        <developer>
            <id>francesco_gallo</id>
            <name>Francesco Gallo</name>
            <email>francesco.gallo@univaq.it</email>
            <url>http://www.di.univaq.it/francesco.gallo/</url>
            <organization>University of LAquila</organization>
            <organizationUrl>http://www.univaq.it/en/</organizationUrl>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>Italy/Rome</timezone>
        </developer>
        <developer>
            <id>claudio_pompilio</id>
            <name>Claudio Pompilio</name>
            <email>claudio.pompilio@graduate.univaq.it</email>
            <organization>University of LAquila</organization>
            <organizationUrl>http://www.univaq.it/en/</organizationUrl>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>Italy/Rome</timezone>
        </developer>
        <developer>
			<id>boulouk</id>
			<name>Georgios Bouloukakis</name>
			<email>boulouk@gmail.com</email>
			<organization>Inria MiMove Team</organization>
			<organizationUrl>https://mimove.inria.fr</organizationUrl>
			<roles>
				<role>developer</role>
			</roles>
            <timezone>France/Paris</timezone>
		</developer>
		<developer>
            <id>frederic_motte</id>
            <name>Frederic Motte</name>
            <email>frederic.motte@thalesgroup.com</email>
            <organization>Thales communications and security</organization>
            <organizationUrl>https://www.thalesgroup.com/</organizationUrl>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>France/Paris</timezone>
        </developer>
    </developers>

  <url>http://www.chorevolution.eu</url>

  <inceptionYear>2015</inceptionYear>
   
   <licenses>
      <license>
         <name>The Apache Software License, Version 2.0</name>
         <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
         <distribution>repo</distribution>
      </license>
   </licenses>

   <scm>
      <connection>scm:git:ssh://gitolite@tuleap.ow2.org/chorevolution/modeling-notations.git</connection>
      <developerConnection>scm:git:ssh://gitolite@tuleap.ow2.org/chorevolution/modeling-notations.git</developerConnection>
      <url>https://tuleap.ow2.org/plugins/git/chorevolution/modeling-notations</url>
      <tag>HEAD</tag>
   </scm>
  
   <issueManagement>
      <system>jira</system>
      <url>https://jira.ow2.org/browse/CRV</url>
   </issueManagement>
   
   <properties>
      <tycho-version>1.1.0</tycho-version>
      <maven.compiler.source>1.8</maven.compiler.source>
      <maven.compiler.target>1.8</maven.compiler.target>
      <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
      
      <eclipse.stream>oxygen</eclipse.stream>
      <sirius.version>5.1.1</sirius.version>
      <eclipse.url>http://download.eclipse.org/releases/${eclipse.stream}</eclipse.url>
      <sirius.url>http://download.eclipse.org/sirius/updates/releases/${sirius.version}/${eclipse.stream}/</sirius.url>
   </properties>
   <distributionManagement>
    <repository>
        <id>central</id>
        <name>http://maven.inria.fr-releases</name>
        <url>http://maven.inria.fr/artifactory/zefxis-public-release</url>
    </repository>
		<snapshotRepository>
        <id>snapshots</id>
        <name>http://maven.inria.fr-snapshots</name>
        <url>http://maven.inria.fr/artifactory/zefxis-public-snapshot</url>
    </snapshotRepository>
	</distributionManagement>
	
   
   <repositories>
      <!-- configure p2 repository to resolve against -->
      <repository>
            <id>eclipse</id>
            <layout>p2</layout>
            <url>${eclipse.url}</url>
        </repository>
      
      <repository>
            <id>sirius</id>
            <layout>p2</layout>
            <url>${sirius.url}</url>
        </repository>
   </repositories>
   
   <!-- the modules that shall be built together in one reactor -->
   <modules>
      <module>plugins/eu.chorevolution.modelingnotations.gidl</module>
      <module>plugins/eu.chorevolution.modelingnotations.gidl.edit</module>
      <module>plugins/eu.chorevolution.modelingnotations.gidl.editor</module>
      <module>features/eu.chorevolution.modelingnotations.gidl.feature</module>
   </modules>
   
   <build>
      <plugins>
         <plugin>
            <groupId>org.eclipse.tycho</groupId>
            <artifactId>tycho-maven-plugin</artifactId>
            <version>${tycho-version}</version>
            <extensions>true</extensions>
         </plugin>
<!--
         <plugin>
            <groupId>org.eclipse.tycho</groupId>
            <artifactId>tycho-source-plugin</artifactId>
            <version>${tycho-version}</version>
            <executions>
               <execution>
                  <id>plugin-source</id>
                  <goals>
                     <goal>plugin-source</goal>
                  </goals>
               </execution>
            </executions>
         </plugin>

         <plugin>
            <groupId>org.eclipse.tycho.extras</groupId>
            <artifactId>tycho-source-feature-plugin</artifactId>
            <version>${tycho-version}</version>
            <executions>
               <execution>
                  <id>source-feature</id>
                  <phase>package</phase>
                  <goals>
                     <goal>source-feature</goal>
                  </goals>
                  <configuration>
                     <excludes>-->
                        <!-- These are bundles and feature that do not have a corresponding source version; NOT the ones that we do not want source versions -->
<!--                        <feature id="org.eclipse.sdk" />
                     </excludes>
                  </configuration>
               </execution>
            </executions>
         </plugin>
-->
         <plugin>
            <groupId>org.eclipse.tycho</groupId>
            <artifactId>target-platform-configuration</artifactId>
            <version>${tycho-version}</version>
            <configuration>
               <pomDependencies>consider</pomDependencies>
               <environments>
                  <environment>
                     <os>linux</os>
                     <ws>gtk</ws>
                     <arch>x86</arch>
                  </environment>
                  <environment>
                     <os>linux</os>
                     <ws>gtk</ws>
                     <arch>x86_64</arch>
                  </environment>
                  <environment>
                     <os>win32</os>
                     <ws>win32</ws>
                     <arch>x86</arch>
                  </environment>
                  <environment>
                     <os>win32</os>
                     <ws>win32</ws>
                     <arch>x86_64</arch>
                  </environment>
                  <environment>
                     <os>macosx</os>
                     <ws>cocoa</ws>
                     <arch>x86_64</arch>
                  </environment>
               </environments>
            </configuration>
         </plugin>
         <plugin>
            <groupId>org.eclipse.tycho</groupId>
            <artifactId>tycho-p2-repository-plugin</artifactId>
            <version>${tycho-version}</version>
            <configuration>
               <includeAllDependencies>true</includeAllDependencies>
            </configuration>
         </plugin>
         
      </plugins>
   </build>
</project>
